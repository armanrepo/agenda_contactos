package com.enfocat.mvc;

import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import com.mysql.jdbc.Connection;



public class DBDatos {
    
    private static final String TABLE = "contactos";
    private static final String TABLECALL = "llamadas";
    private static final String KEY = "id"; 
    private static final String KEYCALL = "id";
   
    public static Contacto newContacto(Contacto cn){
            // NO!!!! String sql =  "INSERT INTO contactos (nombre, email) VALUES ('"+cn.getNombre()+"','"+cn.getEmail()+"')";
            String sql =  "INSERT INTO contactos (nombre, email, telefono) VALUES (?,?,?)";
           
            try (Connection conn = DBConn.getConn();
                 PreparedStatement pstmt = conn.prepareStatement(sql);
                 Statement stmt = conn.createStatement()) {
                
                pstmt.setString(1, cn.getNombre());
                pstmt.setString(2, cn.getEmail());
                //MODIF
                pstmt.setString(3, cn.getTelefono());
              
                pstmt.executeUpdate(); 
            
                //usuario nuevo, actualizamos el ID con el recién insertado
                ResultSet rs = stmt.executeQuery("select last_insert_id()"); 
                if (rs.next()) { 
                    cn.setId(rs.getInt(1));
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            return cn;
    }


  
    public static Contacto getContactoId(int id){
        Contacto cn = null;
        String sql = String.format("select %s,nombre,email,telefono=? from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                cn = new Contacto(
                (Integer) rs.getObject(1),
                (String) rs.getObject(2),
                (String) rs.getObject(3),
                (String) rs.getObject(4)
                );
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return cn;
    }
    





    public static boolean updateContacto(Contacto cn){
        boolean updated = false;
        String sql = String.format("UPDATE contactos SET nombre=?, email=?, telefono=? where id=%d", cn.getId());
    
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            
            pstmt.setString(1, cn.getNombre());
            pstmt.setString(2, cn.getEmail());
            pstmt.executeUpdate(); 
            updated = true;
        
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return updated;
    }

    public static boolean deleteContactoId(int id){
        boolean deleted = false;
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
            deleted=true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return deleted;
    }

    


    public static List<Contacto> getContactos() {
        List<Contacto> listaContactos = new ArrayList<Contacto>();  
        String sql = "select id,nombre,email,telefono from contactos"; 
        try (Connection conn = DBConn.getConn();
            Statement stmt = conn.createStatement()) { 
                
                ResultSet rs = stmt.executeQuery(sql);  
                while (rs.next()) {
                    Contacto u = new Contacto(
                                (Integer) rs.getObject(1),
                                (String) rs.getObject(2),
                                (String) rs.getObject(3),
                                (String) rs.getObject(4)); 
                    listaContactos.add(u);
                    }
        
        } catch (Exception e) { 
        String s = e.getMessage();
        System.out.println(s);
        }
        return listaContactos; 
    }
    
    //Llamadas---------------------------------------------------------------------------------------//

    public static Llamada newLlamada(Llamada cn){
        // NO!!!! String sql =  "INSERT INTO contactos (nombre, email) VALUES ('"+cn.getNombre()+"','"+cn.getEmail()+"')";
        String sql =  "INSERT INTO llamadas (notas, fecha) VALUES (?,?)";
       
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
        	
        	
            pstmt.setString(1, cn.getNotas());
            
            long ms = cn.getFecha().getTime();//con todo este script puedo coger un DATE
            java.sql.Date fecha_sql = new java.sql.Date(ms);
            pstmt.setDate(2, fecha_sql);

            //MODIF
         
            pstmt.executeUpdate(); 
        
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()"); 
            
            if (rs.next()) { 
                cn.setId(rs.getInt(1));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return cn;
}

    public static Llamada getLlamadaId(int id){
        Llamada cn = null;
        String sql = String.format("select %s,notas,fecha from %s where %s=%d", KEYCALL, TABLECALL, KEYCALL, id);
        
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                cn = new Llamada(
                (Integer) rs.getObject(1),
                (String) rs.getObject(2),
                (Date) rs.getObject(3)
                );
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return cn;
    }
    
    public static boolean updateLlamada(Llamada cn){
        boolean updated = false;
        String sql = String.format("UPDATE llamadas set notas=?, fecha=? where id=%d", cn.getId());
    
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)) {
            
            pstmt.setString(1, cn.getNotas());
            //pstmt.setDate(2, cn.getFecha());
            
            long ms = cn.getFecha().getTime();
            java.sql.Date fecha_sql = new java.sql.Date(ms);
            pstmt.setDate(4, fecha_sql);

          
            pstmt.executeUpdate(); 
            updated = true;
        
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return updated;
    }

    public static boolean deleteLlamadaId(int id){
        boolean deleted = false;
        String sql = String.format("DELETE FROM %s where %s=%d", TABLECALL, KEYCALL, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
            deleted=true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return deleted;
    }
    
    public static List<Llamada> getLlamadas() {
        List<Llamada> listaLlamadas = new ArrayList<Llamada>();  
        String sql = "select id,notas,fecha from llamadas"; 
        try (Connection conn = DBConn.getConn();
            Statement stmt = conn.createStatement()) { 
                
                ResultSet rs = stmt.executeQuery(sql);  
                while (rs.next()) {
                    Llamada u = new Llamada(
                            (Integer) rs.getObject(1),
                            (String) rs.getObject(2),
                            (Date) rs.getObject(3));
                                
                    listaLlamadas.add(u);
                    }
        
        } catch (Exception e) { 
        String s = e.getMessage();
        System.out.println(s);
        }
        return listaLlamadas; 
    }
}

