package com.enfocat.mvc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Datos {
    
    private static List<Contacto> contactos = new ArrayList<Contacto>();
    private static List<Llamada> llamadas = new ArrayList<Llamada>();
    
    private static int ultimoContacto = 0;
    private static int ultimaLlamada = 0;

    static {
        contactos.add(new Contacto(1,"ana", "ana@gmail.com","656676222", "avatar1.jpg", "foto1"));
        contactos.add(new Contacto(2,"joan", "joan@gmail.com","655776322", "avatar2.jpg", "foto2"));
        
        Date myDate = new Date();
        
        llamadas.add(new Llamada(1,"joan@gmail.com",myDate));
        llamadas.add(new Llamada(2,"joan@gmail.com",myDate));
        
        ultimoContacto=2;
        ultimaLlamada=2;
    }



    public static Contacto newContacto(Contacto cn){
        ultimoContacto++;
        cn.setId(ultimoContacto);
        contactos.add(cn);
        return cn;
    }


    public static Contacto getContactoId(int id){
        for (Contacto cn : contactos){
            if (cn.getId()==id){
                return cn;
            }
        }
        return null;
    }

    public static boolean updateContacto(Contacto cn){
        boolean updated = false;
        for (Contacto x : contactos){
            if (cn.getId()==x.getId()){
                //x = cn;
                int idx = contactos.indexOf(x);
                contactos.set(idx,cn);
                updated=true;
                break;
            }
        }
        return updated;
    }


    public static boolean deleteContactoId(int id){
        boolean deleted = false;
        
        for (Contacto x : contactos){
            if (x.getId()==id){
                contactos.remove(x);
                deleted=true;
                break;
            }
        }
        return deleted;
    }

    public static List<Contacto> getContactos() {
        return contactos;
    }
    
    //Llamada
    
    public static Llamada newLlamada(Llamada cn){
        ultimaLlamada++;
        cn.setId(ultimaLlamada);
        llamadas.add(cn);
        return cn;
    }
    
    public static Llamada getLlamadaId(int id){
        for (Llamada call : llamadas){
            if (call.getId()==id){
                return call;
            }
        }
        return null;
    }
    
    public static boolean updateLlamada(Llamada cn){
        boolean updated = false;
        for (Llamada x : llamadas){
            if (cn.getId()==x.getId()){
                //x = cn;
                int idx = llamadas.indexOf(x);
                llamadas.set(idx,cn);
                updated=true;
                break;
            }
        }
        return updated;
    }

    public static boolean deleteLlamadaId(int id){
        boolean deleted = false;
        
        for (Llamada x : llamadas){
            if (x.getId()==id){
                llamadas.remove(x);
                deleted=true;
                break;
            }
        }
        return deleted;
    }
    public static List<Llamada> getLlamadas() {
        return llamadas;
    }
    

}

