package com.enfocat.mvc;

import java.util.List;

public class LlamadaController {

    static final String MODO = "db"; //db per bdd en local

    // getAll devuelve la lista completa
    public static List<Llamada> getAll() {
        if (MODO.equals("db")){
            return DBDatos.getLlamadas();
        }else{
            return Datos.getLlamadas();
        }
        
    }

   


    // getId devuelve un registro
    public static Llamada getContactById(int id) {
    
        if (MODO.equals("db")){
            return DBDatos.getLlamadaId(id);
        }else{
            return Datos.getLlamadaId(id);
        }
        
    }

    // save guarda un Contacto
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void save(Llamada cn) {

        if (MODO.equals("db")){
            if (cn.getId() > 0) {
                DBDatos.updateLlamada(cn);
            } else {
                DBDatos.newLlamada(cn);
            }
        }else{
            if (cn.getId() > 0) {
                Datos.updateLlamada(cn);
            } else {
                Datos.newLlamada(cn);
            }
        }
        
        
    }

    // size devuelve numero de Contactos
    public static int size() {
        
        if (MODO.equals("db")){
            return DBDatos.getLlamadas().size();
        }else{
            return Datos.getLlamadas().size();
        }
    }

    // removeId elimina Contacto por id
    public static void removeId(int id) {
        
        if (MODO.equals("db")){
            DBDatos.deleteLlamadaId(id);
        }else{
            Datos.deleteLlamadaId(id);
        }
    }

}