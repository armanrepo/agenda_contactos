package com.enfocat.mvc;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Llamada {
  
    private int id;
    private String notas;
    private Date fecha;
    private String sfecha;
 
    
   
    // CONSTRUCTOR SIN ID

    //CONSTRUCTOR CON ID
    
    public Llamada(int id, String notas,Date fecha) {
        this.id = id;
        this.notas = notas;
        this.fecha = fecha;
    }
    
    public Llamada(String notas, String sfecha) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			this.fecha = sdf.parse(sfecha);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.notas = notas;
	}
    public Llamada(int id,String notas, String sfecha) {
		
  		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
  		
  		try {
  			this.fecha = sdf.parse(sfecha);
  		} catch (ParseException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
  		
  		this.notas = notas;
  		this.id = id;
  	}



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
	public String getSfecha() {
		return sfecha;
	}

	public void setSfecha(String sfecha) {
		this.sfecha = sfecha;
	}

    
  
}